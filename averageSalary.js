// 6. Find the average salary of based on country using only HOF method
const data = require("./data");
const fs = require("fs");
const sumOfSalariesCountry = require("./sumOfSalariesCountry");

function averageSalaries(data) {
  let sumOfSalariesInCountry = sumOfSalariesCountry(data);

  sumOfSalariesInCountry = Object.entries(sumOfSalariesInCountry);

  return sumOfSalariesInCountry
    .map((data) => {
      data[1].average_salary = data[1].total_salary / data[1].numberOfSalaries;
      return data;
    })
    .reduce((acc, curr) => {
      if (!acc[curr[0]]) {
        acc[curr[0]] = curr[1];
      }
      return acc;
    }, {});
}
fs.writeFileSync(
  "./Output/averageSalaries.json",
  JSON.stringify(averageSalaries(data)),
  (error) => {
    console.log(error);
  }
);

// 2.Convert all the salary values into proper numbers instead of strings
const data = require("./data");
const fs = require("fs");

function properNumbers(data) {
  return data.map((data) => {
    let salary = parseFloat(data.salary.slice(1));
    data["salary"] = salary;
    return data;
  });
}
fs.writeFileSync(
  "./Output/properNumbers.json",
  JSON.stringify(properNumbers(data)),
  (error) => {
    console.log(error);
  }
);

//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const data = require("./data");
const fs = require("fs");

function factor(data) {
  return data.map((data) => {
    let corrected_salary = parseFloat(data.salary.slice(1)) * 10000;
    data["corrected_salary"] = corrected_salary;
    return data;
  });
}
fs.writeFileSync(
  "./Output/factorOf10000.json",
  JSON.stringify(factor(data)),
  (error) => {
    console.log(error);
  }
);

module.exports = factor;

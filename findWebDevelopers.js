const data = require("./data");
const fs = require("fs");

// 1. Find all Web Developers
function findWebDevelopers(data) {
  return data.filter((data) => {
    if (data.job.slice(0, 13) == "Web Developer") {
      return data;
    }
  });
}
fs.writeFileSync(
  "./Output/findWebDeveloper.json",
  JSON.stringify(findWebDevelopers(data)),
  (error) => {
    console.log(error);
  }
);

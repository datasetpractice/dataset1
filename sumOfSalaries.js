// 4. Find the sum of all salaries
const data = require("./data");
const fs = require("fs");
const factorOf1000 = require("./factorOf1000");

function sumOfSalaries(data) {
  let correctedSalary = factorOf1000(data);
  return correctedSalary.reduce((acc, curr) => {
    acc += curr.corrected_salary;
    return acc;
  }, 0);
}
fs.writeFileSync(
  "./Output/sumOfSalaries.json",
  JSON.stringify(sumOfSalaries(data)),
  (error) => {
    console.log(error);
  }
);

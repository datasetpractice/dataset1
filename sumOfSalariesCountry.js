const data = require("./data");
const fs = require("fs");
const factorOf1000 = require("./factorOf1000");

// 5. Find the sum of all salaries based on country using only HOF method
function sumOfSalariesCountry(data) {
  let correctedData = factorOf1000(data);
  return correctedData.reduce((acc, curr) => {
    if (acc[curr.location]) {
      acc[curr.location]["total_salary"] += curr.corrected_salary;
      acc[curr.location]["numberOfSalaries"] += 1;
    } else {
      acc[curr.location] = {};
      acc[curr.location].total_salary = curr.corrected_salary;
      acc[curr.location]["numberOfSalaries"] = 1;
    }
    return acc;
  }, {});
}
fs.writeFileSync(
  "./Output/sumOfSalariesCountry.json",
  JSON.stringify(sumOfSalariesCountry(data)),
  (error) => {
    console.log(error);
  }
);
module.exports = sumOfSalariesCountry;
